﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{

    [Authorize]
    [HandleError(ExceptionType = typeof(Exception), View = "Error")]
    public class BaseController : Controller
    {
        protected SunbeamDBEntities dbObj { get; set; }
        public BaseController()
        {
            this.dbObj = new SunbeamDBEntities();
        }
    }
}