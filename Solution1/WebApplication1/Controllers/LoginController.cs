﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class LoginController : Controller
    {
        SunbeamDBEntities dbObj = new SunbeamDBEntities();
        public ActionResult SignIn()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SignIn(EmployeeDetail obj1, string ReturnUrl)
        {
            int MatchUser = (from hr in dbObj.EmployeeDetails.ToList()
                             where hr.username.ToLower() == obj1.username.ToLower()
                             && hr.password == obj1.password
                             select hr).ToList().Count();
            if (MatchUser == 1)
            {
                FormsAuthentication.SetAuthCookie(obj1.username, false);
                if (ReturnUrl != null)
                {
                    return Redirect(ReturnUrl);
                }
                else
                {
                    return Redirect("/Home/Index");
                }
            }
            else
            {
                ViewBag.ErrorMessage = "Username or password is wrong";
                return View();
            }
        }

        public ActionResult Signout()
        {
            FormsAuthentication.SignOut();
            return Redirect("/Home/Index");
        }
    }
}