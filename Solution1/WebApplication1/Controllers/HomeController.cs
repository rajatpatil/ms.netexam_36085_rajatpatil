﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class HomeController : BaseController
    {
        // GET: Home
        public ActionResult Index()
        {
            ViewBag.MyTitle = "Welcome Home";
            ViewBag.UserName = User.Identity.Name;

            var allEmps = dbObj.EmployeeDetails.ToList();
            return View(allEmps);
        }
        public ActionResult ListLeave()
        {
            ViewBag.MyTitle = "List Leave";
            ViewBag.UserName = User.Identity.Name;

            var allEmps = dbObj.LeaveApplicationDatas.ToList();
            return View(allEmps);
        }
        public ActionResult Create()
        {

            ViewBag.UserName = User.Identity.Name;

            return View();

        }

        [HttpPost]
        public ActionResult Create(EmployeeDetail empToBeAdded)
        {

            dbObj.EmployeeDetails.Add(empToBeAdded);
            dbObj.SaveChanges();

            return Redirect("/Home/Index");

        }
        public ActionResult Edit(int id)
        {

            ViewBag.Message = "Please Update Record Here!";
            ViewBag.UserName = User.Identity.Name;

            EmployeeDetail empToBeEdited = (from emp in dbObj.EmployeeDetails.ToList()
                                 where emp.Emp_id == id
                                 select emp).First();

            return View(empToBeEdited);

        }

         [HttpPost]
         public ActionResult Edit(EmployeeDetail empUpdated)
         {
         
             var v = ViewBag.Message;
             EmployeeDetail empToBeEdited = (from emp in dbObj.EmployeeDetails.ToList()
                                  where emp.Emp_id == empUpdated.Emp_id
                                             select emp).First();
            dbObj.EmployeeDetails.Remove(empToBeEdited);
            dbObj.EmployeeDetails.Add(empUpdated);
            dbObj.SaveChanges();//This will update the database
         
             return Redirect("/Home/Index");
         
         }
        public ActionResult CreateLeave()
        {

            ViewBag.UserName = User.Identity.Name;

            return View();

        }

        [HttpPost]
        public ActionResult CreateLeave(LeaveApplicationData empToBeAdded)
        {

            dbObj.LeaveApplicationDatas.Add(empToBeAdded);
            dbObj.SaveChanges();

            return Redirect("/Home/Index");

        }

    }
}